/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.studica.frc.TitanQuad;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class Drivetrain extends SubsystemBase {
  /**
   * Creates a new Drivetrain.
   */

  private TitanQuad leftFront;
  private TitanQuad leftBack;
  private TitanQuad rightFront;
  private TitanQuad rightBack;

  public Drivetrain() {
    leftFront = new TitanQuad(Constants.TITAN_QUAD, Constants.LEFT_FRONT);
    leftBack = new TitanQuad(Constants.TITAN_QUAD, Constants.LEFT_BACK);
    rightFront = new TitanQuad(Constants.TITAN_QUAD, Constants.RIGHT_FRONT);
    rightBack = new TitanQuad(Constants.TITAN_QUAD, Constants.RIGHT_BACK);


    rightFront.setInverted(true);
    rightBack.setInverted(true);
  }

  public void mecanumDrive(double speedLR, double rotation, double speedFB) {
    leftFront.set(speedFB + speedLR + rotation);
    leftBack.set(speedFB - speedLR + rotation);
    rightFront.set(speedFB - speedLR - rotation);
    rightBack.set(speedFB + speedLR - rotation);
  }

  public void stop() {
    leftFront.stopMotor();
    leftBack.stopMotor();
    rightFront.stopMotor();
    rightBack.stopMotor();
  }
}
