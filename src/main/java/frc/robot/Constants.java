/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants.  This class should not be used for any other purpose.  All constants should be
 * declared globally (i.e. public static).  Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {
    public static final int TITAN_QUAD = 42;

    public static final int LEFT_BACK = 0;
    public static final int LEFT_FRONT = 1;
    public static final int RIGHT_BACK = 2;
    public static final int RIGHT_FRONT = 3;

    //Radius of drive wheel in mm
    public static final int wheelRadius = 95;

    //Encoder pulses per rotation of motor shaft
    public static final int pulsePerRotation = 1440;

    //Gear ratio between motor shaft and output shaft
    public static final double gearRatio = 1/1;

    //Pulse per rotation combined with gear ratio
    public static final double encoderPulseRatio = pulsePerRotation * gearRatio;

    //Distance per tick
    public static final double distancePerTick = (Math.PI * 2 * wheelRadius) / encoderPulseRatio;
}
