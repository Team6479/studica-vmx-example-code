# Studica VMX Example code

This repository contains example code for a Studica VMX FRC-compatible Training Bot.

## Required Visual Studio Code Extensions

| Extensions |
| ------ |
| WPILib |
| Language Support for Java |
| VMX-Pi WPIlib  |

These extensions can be installed through the Extensions menu.

# Building and Deploying

Connect to the robot's WiFi access point.

Inside VS Code, do <kbd>ctrl</kbd> + <kbd>shift</kbd> + <kbd>p</kbd>, and search for `WPIlib: Deploy Robot Code` to deploy the current code to the robot.

# Creating a new Project for the VMX

Do <kbd>ctrl</kbd> + <kbd>shift</kbd> + <kbd>p</kbd>, and search for `WPIlib: Create a new Project`.

Go through the menu, selecting `Template` as the project type, `java` as the language, and `Command Robot` as the project base.

Choose a folder to create the project in, and open it with the dialog.


Once in the new project directory, do <kbd>ctrl</kbd> + <kbd>shift</kbd> + <kbd>p</kbd> and search for `WPIlib: Manage Vendor Libraries`.

Select `Install new libraries (online)`, and paste the link below to install the Titan Quad vendor library.

```
http://dev.studica.com/releases/2020/Studica.json
```

Other vendor libraries can be installed in the same way, links for which can be found online with a bit of searching.

Once it has completed, we need to change the project type to be compatible with the VMX-Pi.

Do <kbd>ctrl</kbd> + <kbd>shift</kbd> + <kbd>p</kbd> again, and search for `VMX-Pi: Change the deploy target to VMX-Pi (from RoboRIO)` (this will not work unless the VMX-Pi WPIlib extension is installed)

After this, do <kbd>ctrl</kbd> + <kbd>shift</kbd> + <kbd>p</kbd> and find `WPIlib: Build Robot Code` to build the code.

Wait for it to build, when it will say `BUILD SUCCESSFUL`.

After a build is successful, the robot can be programmed using normal WPIlib practices. The motor controller class `TitanQuad` can be used to interact with the motors on the robot, documentation can be found [here](https://docs.wsr.studica.com/en/latest/docs/Titan/programing-the-titan.html).
